---
title: JSTL标签库
date: 2022/12/25
categories: 参考资料
tags: JSP
description: 常用的JSTL标签
---
## 核心标签库

`<%@ taglib prefix="c" uri="<http://java.sun.com/jsp/jstl/core>" %>`

| 方法 | 用法 |
| --- | --- |
| [\<c:out>](https://www.runoob.com/jsp/jstl-core-out-tag.html) | 用于在JSP中显示数据，就像<%= ... > |
| [\<c:set>](https://www.runoob.com/jsp/jstl-core-set-tag.html) | 用于保存数据 |
| [\<c:remove>](https://www.runoob.com/jsp/jstl-core-remove-tag.html) | 用于删除数据 |
| [\<c:catch>](https://www.runoob.com/jsp/jstl-core-catch-tag.html) | 用来处理产生错误的异常状况，并且将错误信息储存起来 |
| [\<c:if>](https://www.runoob.com/jsp/jstl-core-if-tag.html) | 与我们在一般程序中用的if一样 |
| [\<c:choose>](https://www.runoob.com/jsp/jstl-core-choose-tag.html) | 本身只当做\<c:when>和\<c:otherwise>的父标签 |
| [\<c:when>](https://www.runoob.com/jsp/jstl-core-choose-tag.html) | \<c:choose>的子标签，用来判断条件是否成立 |
| [\<c:otherwise>](https://www.runoob.com/jsp/jstl-core-choose-tag.html) | \<c:choose>的子标签，接在\<c:when>标签后，当\<c:when>标签判断为false时被执行 |
| [\<c:import>](https://www.runoob.com/jsp/jstl-core-import-tag.html) | 检索一个绝对或相对 URL，然后将其内容暴露给页面 |
| [\<c:forEach>](https://www.runoob.com/jsp/jstl-core-foreach-tag.html) | 基础迭代标签，接受多种集合类型 |
| [\<c:forTokens>](https://www.runoob.com/jsp/jstl-core-foreach-tag.html) | 根据指定的分隔符来分隔内容并迭代输出 |
| [\<c:param>](https://www.runoob.com/jsp/jstl-core-param-tag.html) | 用来给包含或重定向的页面传递参数 |
| [\<c:redirect>](https://www.runoob.com/jsp/jstl-core-redirect-tag.html) | 重定向至一个新的URL. |
| [\<c:url>](https://www.runoob.com/jsp/jstl-core-url-tag.html) | 使用可选的查询参数来创造一个URL |

## 格式化标签

 `<%@ taglib prefix="fmt" uri="<http://java.sun.com/jsp/jstl/fmt>" %>`

| 方法 | 用法 |
| --- | --- |
| fmt:formatNumber | 使用指定的格式或精度格式化数字 |
| fmt:parseNumber | 解析一个代表着数字，货币或百分比的字符串 |
| fmt:formatDate | 使用指定的风格或模式格式化日期和时间 |
| fmt:parseDate | 解析一个代表着日期或时间的字符串 |
| fmt:bundle | 绑定资源 |
| fmt:setLocale | 指定地区 |
| fmt:setBundle | 绑定资源 |
| fmt:timeZone | 指定时区 |
| fmt:setTimeZone | 指定时区 |
| fmt:message | 显示资源配置文件信息 |
| fmt:requestEncoding | 设置request的字符编码 |

## sql标签

`<%@ taglib prefix="sql" uri="<http://java.sun.com/jsp/jstl/sql>" %>`

| 方法 | 用法 |
| --- | --- |
| sql:setDataSource | 指定数据源 |
| sql:query | 运行SQL查询语句 |
| sql:update | 运行SQL更新语句 |
| sql:param | 将SQL语句中的参数设为指定值 |
| sql:dateParam | 将SQL语句中的日期参数设为指定的java.util.Date 对象值 |
| sql:transaction | 在共享数据库连接中提供嵌套的数据库行为元素，将所有语句以一个事务的形式来运行 |

## xml标签

`<%@ taglib prefix="x" uri="<http://java.sun.com/jsp/jstl/xml>" %>`

| 方法 | 用法 |
| --- | --- |
| [\<x:out>](https://www.runoob.com/jsp/jstl-xml-out-tag.html) | 与<%= ... >,类似，不过只用于XPath表达式 |
| [\<x:parse>](https://www.runoob.com/jsp/jstl-xml-parse-tag.html) | 解析 XML 数据 |
| [\<x:set>](https://www.runoob.com/jsp/jstl-xml-set-tag.html) | 设置XPath表达式 |
| [\<x:if>](https://www.runoob.com/jsp/jstl-xml-if-tag.html) | 判断XPath表达式，若为真，则执行本体中的内容，否则跳过本体 |
| [\<x:forEach>](https://www.runoob.com/jsp/jstl-xml-foreach-tag.html) | 迭代XML文档中的节点 |
| [\<x:choose>](https://www.runoob.com/jsp/jstl-xml-choose-tag.html) | \<x:when>和\<x:otherwise>的父标签 |
| [\<x:when>](https://www.runoob.com/jsp/jstl-xml-choose-tag.html) | \<x:choose>的子标签，用来进行条件判断 |
| [\<x:otherwise>](https://www.runoob.com/jsp/jstl-xml-choose-tag.html) | \<x:choose>的子标签，当\<x:when>判断为false时被执行 |
| [\<x:transform>](https://www.runoob.com/jsp/jstl-xml-transform-tag.html) | 将XSL转换应用在XML文档中 |
| [\<x:param>](https://www.runoob.com/jsp/jstl-xml-param-tag.html) | 与\<x:transform>共同使用，用于设置XSL样式表 |

## jstl函数

`<%@ taglib prefix="fn" uri="<http://java.sun.com/jsp/jstl/functions>" %>`

| 方法 | 用法 |
| --- | --- |
| [fn:contains()](https://www.runoob.com/jsp/jstl-function-contains.html) | 测试输入的字符串是否包含指定的子串 |
| [fn:containsIgnoreCase()](https://www.runoob.com/jsp/jstl-function-containsignoreCase.html) | 测试输入的字符串是否包含指定的子串，大小写不敏感 |
| [fn:endsWith()](https://www.runoob.com/jsp/jstl-function-endswith.html) | 测试输入的字符串是否以指定的后缀结尾 |
| [fn:escapeXml()](https://www.runoob.com/jsp/jstl-function-escapexml.html) | 跳过可以作为XML标记的字符 |
| [fn:indexOf()](https://www.runoob.com/jsp/jstl-function-indexof.html) | 返回指定字符串在输入字符串中出现的位置 |
| [fn:join()](https://www.runoob.com/jsp/jstl-function-join.html) | 将数组中的元素合成一个字符串然后输出 |
| [fn:length()](https://www.runoob.com/jsp/jstl-function-length.html) | 返回字符串长度 |
| [fn:replace()](https://www.runoob.com/jsp/jstl-function-replace.html) | 将输入字符串中指定的位置替换为指定的字符串然后返回 |
| [fn:split()](https://www.runoob.com/jsp/jstl-function-split.html) | 将字符串用指定的分隔符分隔然后组成一个子字符串数组并返回 |
| [fn:startsWith()](https://www.runoob.com/jsp/jstl-function-startswith.html) | 测试输入字符串是否以指定的前缀开始 |
| [fn:substring()](https://www.runoob.com/jsp/jstl-function-substring.html) | 返回字符串的子集 |
| [fn:substringAfter()](https://www.runoob.com/jsp/jstl-function-substringafter.html) | 返回字符串在指定子串之后的子集 |
| [fn:substringBefore()](https://www.runoob.com/jsp/jstl-function-substringbefore.html) | 返回字符串在指定子串之前的子集 |
| [fn:toLowerCase()](https://www.runoob.com/jsp/jstl-function-tolowercase.html) | 将字符串中的字符转为小写 |
| [fn:toUpperCase()](https://www.runoob.com/jsp/jstl-function-touppercase.html) | 将字符串中的字符转为大写 |
| [fn:trim()](https://www.runoob.com/jsp/jstl-function-trim.html) | 移除首尾的空白符 |